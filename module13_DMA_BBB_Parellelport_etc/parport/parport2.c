#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/ioport.h>
#include <linux/uaccess.h>
#include <linux/sched.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/workqueue.h>
#include <asm/signal.h>
#include <asm/irq_vectors.h>

#define baseaddr 0x378	//parallel port address. check datasheet

MODULE_LICENSE("DUAL BSD/GPL");

static int led_open(struct inode *,struct file *);
static int led_release(struct inode *,struct file *);
static ssize_t led_write(struct file *,const char __user *,size_t,loff_t *);
//irq_handler_t led_isr(int,void *,struct pt_regs*); //interrupt handler

int result,err;
dev_t devno;
int led_major=99,led_minor=0;	//static major no, since parport is standard device
int irqno = 7,x=4;

//device structure
struct led_dev
{
	char kbuf[10];
//	int curr;
//	int max_size;
	struct cdev cdev;	//embed the cdev struct in device struct	
};

struct led_dev mydev =
{
	.kbuf = '\0',
//	.curr = 0,
//	.max_size=20,
};

//file operation structure fs.h
struct file_operations led_fops = 
{
	.owner = THIS_MODULE,
	.open = led_open,
	.write = led_write,
	.release = led_release,
};
/*
//interrupt handler
irq_handler_t led_isr(int irq,void *dev_id,struct pt_regs *regs)
{
	printk(KERN_ALERT"In interrupt handler\n");
	return IRQ_HANDLED;
}
*/
static int hello_init(void)
{
	unregister_chrdev_region(devno,1);	//major
	printk(KERN_INFO"Inserting module ioport\n");
	if(led_major > 0)	//if static major no, here 99 for parport
	{
		devno = MKDEV(led_major,led_minor);	//make device no out of major no and minor no.
		result =  register_chrdev_region(devno,1,"my_led");
		printk(KERN_INFO"in if condition, dno=%d\n",devno);
		printk("major : %d,minor : %d\n",MAJOR(devno),MINOR(devno));
	}
	else
	{
		result = alloc_chrdev_region(&devno,led_minor,1,"led");
		led_major = MAJOR(devno);
		printk(KERN_WARNING"major : %d,minor : %d\n",led_major,led_minor);
	}
	if(result < 0)
		printk(KERN_WARNING"can't get major number\n");

	cdev_init(&mydev.cdev,&led_fops);	//initialize cdev as charecter device
	mydev.cdev.owner = THIS_MODULE;

	err = cdev_add(&mydev.cdev,devno,1);	//add to kernel
	if(err)
	{
		printk(KERN_NOTICE"error %d adding \n",err);
		return -1;
	}

/*	err = check_region(baseaddr,1);
	if(err<0)
		return err;
*/
//	request_region(baseaddr,1,"my_parport");

/*	if(request_irq(irqno,(void *)led_isr,IRQF_SHARED,"led_test",&x))
	{
		printk(KERN_ERR"device cannot register for interrupt %d\n",irqno);
		return -EIO;
	}
*/	return 0;
}

static int led_open(struct inode *inode,struct file *filp)
{
	struct led_dev *dev;
	dev = container_of(inode -> i_cdev,struct led_dev,cdev);	//obtain pointer to structure
	filp->private_data = dev;					//file pointer has now acces to resources

	printk(KERN_INFO "open module\n");
	if((filp -> f_flags&O_ACCMODE) == O_WRONLY)
	{
		printk(KERN_INFO "module opened for write only\n");
	}
	else if ((filp->f_flags&O_ACCMODE) == O_RDONLY)
	{
		printk(KERN_INFO "module opened for read only\n");
	}
	else
	{
		printk(KERN_WARNING "mode not supported\n");
		return -1;
	}
	printk("in open function, enabling interrupt\n");

	//outb(1<<4,baseaddr+2);
	outb(0x10,baseaddr+2);

	return 0;
}

static ssize_t led_write(struct file *filp,const char __user *ubuf,size_t size,loff_t *off)
{
	int i;
	struct led_dev *dev;
	dev = (struct led_dev *)filp -> private_data;	//now file pointer has access to our resources
	printk("in led_write\n");

	for(i=0;i<size;i++)
	{
		outb(ubuf[i],baseaddr);
	}
	printk("ubuf : %s\n size : %ld\n",ubuf,size);
	return size;
}

static int led_release(struct inode *inode,struct file *filp)
{
	printk(KERN_ALERT"closing file i.e releasing module\n");
	return 0;
}

static void hello_exit(void)
{
	printk(KERN_ALERT "cleanup module ioport\n");
//	free_irq(irqno,&x);
//	release_region(baseaddr,1);	//only 1 byte
	cdev_del(&mydev.cdev);
	unregister_chrdev_region(devno,1);	//major
}

module_init(hello_init);
module_exit(hello_exit);
MODULE_LICENSE("GPL");
