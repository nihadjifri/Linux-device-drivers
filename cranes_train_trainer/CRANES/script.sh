#!/bin/bash

echo "Setting up the Directory"
cd ~
mkdir ~/BBB
cd ~/BBB

echo "Obtaining ARM Cross Compiler : GCC"
wget -c https://releases.linaro.org/components/toolchain/binaries/6.4-2018.05/arm-linux-gnueabihf/gcc-linaro-6.4.1-2018.05-x86_64_arm-linux-gnueabihf.tar.xz
tar xf gcc-linaro-6.4.1-2018.05-x86_64_arm-linux-gnueabihf.tar.xz
export CC=`pwd`/gcc-linaro-6.4.1-2018.05-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-
${CC}gcc --version

echo "Obtaining Bootloader : U-Boot"
git clone https://github.com/u-boot/u-boot
cd ~/BBB/u-boot/
git checkout v2018.09 -b tmp

echo "Obtaining Patches"
wget -c https://rcn-ee.com/repos/git/u-boot-patches/v2018.09/0001-am335x_evm-uEnv.txt-bootz-n-fixes.patch
wget -c https://rcn-ee.com/repos/git/u-boot-patches/v2018.09/0002-U-Boot-BeagleBone-Cape-Manager.patch

echo "Applying Patches"
patch -p1 < 0001-am335x_evm-uEnv.txt-bootz-n-fixes.patch
patch -p1 < 0002-U-Boot-BeagleBone-Cape-Manager.patch

echo "Configure and Build"
make ARCH=arm CROSS_COMPILE=${CC} distclean
make ARCH=arm CROSS_COMPILE=${CC} am335x_evm_defconfig
make ARCH=arm CROSS_COMPILE=${CC}

cd ~/BBB

echo "Obtaining Linux Kernel"
git clone https://github.com/RobertCNelson/bb-kernel
cd ~/BBB/bb-kernel/
git checkout origin/am33x-v4.9 -b tmp

echo "BUILDING KERNEL"
./build_kernel.sh
