#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<stdlib.h>
#include<fcntl.h>
int main(int argc,char* argv[])
{
	if(argc < 2)
	{
		printf("SYNTAX: %s <devNodeName>\n",argv[0]);
	}
	int val = 0;
	char buff[10];
	int fd = open(argv[1],O_RDWR);
	if(fd == -1)
	{
		perror("Open :");
		return -1;
	}
	do{
		printf("0 - Exit\n");
		printf("1 - Read\n");
		printf("2 - Write\n");
		scanf("%d",&val);
		switch(val)
		{
			case 0:
				close(fd);
				exit(0);
				break;
			case 1:
				read(fd,buff,2);
				break;
			case 2:
				write(fd,"hello",5);
				break;
			default:
				break;
		}
	}while(1);

	return 0;
}
