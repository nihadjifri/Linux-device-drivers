#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

static int __init hellowolrd_init(void) {
	    pr_info("Hello world!\n");

	    /*
	     * printk(KERN_ERR "This is an error\n");
	     * pr_emerg, pr_alert, pr_crit, pr_err, pr_warning, pr_notice, pr_info, and pr_debug:
	    */
	        return 0;
}

static void __exit hellowolrd_exit(void) {
	    pr_info("End of the world\n");
}

module_init(hellowolrd_init);
module_exit(hellowolrd_exit);
MODULE_AUTHOR("Shaik Saifulla <shaik4consulting@gmail.com>");
MODULE_LICENSE("GPL");

