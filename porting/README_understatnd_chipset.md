https://www.digikey.com/eewiki/display/linuxonarm/BeagleBoard

Understanding the linux kernel : 

Intel architecture:
https://en.wikipedia.org/wiki/Nehalem_(microarchitecture

cache memory:[l1,l2,l3]
https://en.wikipedia.org/wiki/Cache_hierarchy
https://en.wikipedia.org/wiki/Cache_(computing)
https://en.wikipedia.org/wiki/Cache_coherence
https://en.wikipedia.org/wiki/CPU_cache
https://en.wikipedia.org/wiki/CPU_cache#Multi-level_caches

http://www.ecs.umass.edu/ece/koren/architecture/Cache/tutorial.html
https://www.geeksforgeeks.org/cache-memory/

https://lwn.net/Articles/252125/
https://www.extremetech.com/extreme/188776-how-l1-and-l2-cpu-caches-work-and-why-theyre-an-essential-part-of-modern-chips
http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.faqs/ka8788.html


casche controller:
https://www.ijcaonline.org/research/volume129/number1/chauan-2015-ijca-906787.pdf
http://www.rroij.com/open-access/design-of-cache-memory-with-cache-controller-using-vhdl-.php?aid=46301

MMU:
https://en.wikipedia.org/wiki/Memory_management_unit
TLB:
https://en.wikipedia.org/wiki/Translation_lookaside_buffer
GPU:

DSP:
https://en.wikipedia.org/wiki/Digital_signal_processor

others:
https://www.microarch.org/micro36/html/pdf/beckmann-TLC.pdf

