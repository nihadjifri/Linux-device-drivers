# linux_porting_on_beagle_bone_black

This repo discussess the steps to be followed to port linux on beagle bone black


Basic Requirements:
------------------
Running a recent release of Debian, Fedora or Ubuntu; without OS Virtualization Software.

ARM Cross Compiler – Linaro: http://www.linaro.org

    Linaro Toolchain Binaries: http://www.linaro.org/downloads/

Bootloader

    Das U-Boot – the Universal Boot Loader: http://www.denx.de/wiki/U-Boot
    Source: http://git.denx.de/?p=u-boot.git;a=summary

Linux Kernel

    Linus's Mainline tree: https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git

ARM based rootfs

    Debian: https://www.debian.org
    Ubuntu: http://www.ubuntu.com

Working Beagle bone black (BBB) board, running any of the linux distributions:

checks for working condition:
1-> power on your BBB by pluging to your system.(You will get a device node /dev/ttyACM0)
2-> open browser and redirect the browser to 192.168.7.2 it will take you to a web page hosted on BBB itself
    there you can see a green box displaying "Your board is connected", means every thing works fine.

    Now if you want to confirm the working condition by running some commands you can use cloud9 IDE whose 
    link is provided on the left panel of the site.
3-> You can also verify the working condition by communicating to BBB over serial port(device node - /dev/ttyUSB0)
    For this you can connect the usb to serial converter on the on board jumpers.
    J1 - Black (Gnd)
    J2 - 
    J3 -
    J4 - Green (Rx)
    J5 - White (Tx)
    J6 -

Now use picocom to connect to the device by setting the appropriate Boardrate
$sudo apt install picocom
$sudo picocom -b 115200 /dev/ttyUSB0
default username:password is [debian:temppwd]
Now it will take you to the prompt where you can access your system through command line.

Now follow the steps from :
https://www.digikey.com/eewiki/display/linuxonarm/BeagleBone+Black

