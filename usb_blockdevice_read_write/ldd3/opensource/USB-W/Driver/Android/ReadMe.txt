
==================================================
PL2303 Android SDK v1.0.0.6
For Android 3.2 and above
==================================================

Requirements:
-------------
- USB Device with PL2303HXD, PL2303EA, PL2303RA, PL2303SA, PL2551B controller chip
	NOTE: PL2303HXA and PL2303XA are not supported (Discontinued Products)
- Android-Powered Device (Tablet/Phones) with USB Host API Mode Support 
	- Android 3.2 and above OS versions


PL2303 Android SDK includes:
----------------------------
1. PL2303 Sample Test Android App Software  (ap_PL2303HXDSimpleTest.apk)
	- This is demo application to detect PL2303 device in Android and do simple test.
2. PL2303 Android USB Host Demo AP User Manual 
	- This document which includes User Guide for running the above sample program.
3. PL2303 Sample Test Android App Source Code 
	- Source code for ap_PL2303HXDSimpleTest.apk
4. PL2303 Android Java Driver Library  (pl2303driver.jar)
	- PL2303 Android Driver for PL2303HXD, PL2303EA, PL2303RA, and PL2303SA only.
5. PL2303 Android App Development Reference Document  
	- Reference document for writing Android Application Software (index.html)
6. PL2303HXD Android Host Compatibility List  
	- Compatible Android host devices tested.

 
Release Notes:
---------------
PL2303Android SDK v1.0.0.6
	. Add support for VID_PID:
		. 067B/AAA5
		. 0557/2008
		. 05AD/0FBA
	. Modify Android 4.2 USB host API issue

PL2303Android SDK v1.0.0.5
 	. Added set USB VID/PID API. See included Set_VID_PID.JPG file.
	   - Customer can refer to this API source on how to add their VID/PID.

PL2303Android SDK v1.0.0.4
 	. Fixed Loopback Test function (disabled Write and Read Data button while running).

PL2303Android SDK v1.0.0.3
 	. Added Loopback Test function in Demo AP.

PL2303Android SDK v1.0.0.2
 	. Added 4K buffer size.

PL2303Android SDK v1.0.0.1
 	. Add Read Timeout settings.
	. Fix auto-popup message. 

PL2303Android SDK v1.0.0.0
 	. Initial Formal Release.


========================================
Prolific Technology Inc.
http://www.prolific.com.tw


