https://www.digikey.com/eewiki/display/linuxonarm/Wandboard


Step-1:		cd /home/saif/EmbeddedLinux_Wandboard/Uboot/u-boot

Step-2:		patch -p1 < 0001-wandboard-uEnv.txt-bootz-n-fixes.patch

Step-3:		export CC=/home/saif/EmbeddedLinux_Wandboard/armv7-multiplatform/dl/gcc-linaro-7.3.1-2018.05-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-

Step-4:		${CC}gcc --version

Step-5:		make ARCH=arm CROSS_COMPILE=${CC} distclean

Step-6:		make ARCH=arm CROSS_COMPILE=${CC} wandboard_defconfig

Step-7:		make ARCH=arm CROSS_COMPILE=${CC}

Step-8:		lsblk

Step-9:		export DISK=/dev/sdb

Step-10:		sudo dd if=/dev/zero of=${DISK} bs=1M count=10

Step-11:		sudo dd if=SPL of=${DISK} seek=1 bs=1k

Step-12:		sync

Step-13:		sudo dd if=u-boot.img of=${DISK} seek=69 bs=1k

Step-14:		sync

Step-15:		umount /dev/sdb1

Step-16:		sudo sfdisk ${DISK} <<-__EOF__
1M,,L,*
__EOF__


Step-17:		sudo mkfs.ext4 -L rootfs ${DISK}1

Step-18:		mount  --> check /dev/sdb1 is not mounted

Step-19:		sudo mkdir -p /media/rootfs/

Step-20:		sudo mount ${DISK}1 /media/rootfs/

Step-21:		mount  --> check /dev/sdb1 is mounted

Step-22:		export kernel_version=4.14.71-armv7-x11.1

Step-23:		cd /home/saif/EmbeddedLinux_Wandboard/rootfs

Step-24:		tar xf debian-9.5-minimal-armhf-2018-07-30.tar.xz

Step-25:		sudo tar xfvp ./*-*-*-armhf-*/armhf-rootfs-*.tar -C /media/rootfs/

Step-26:		sync

Step-27:		sync

Step-28:		sudo chown root:root /media/rootfs/

Step-29:		sudo chmod 755 /media/rootfs/

Step-30:		sudo sh -c "echo 'uname_r=${kernel_version}' >> /media/rootfs/boot/uEnv.txt"

Step-31:		sudo sh -c "echo 'cmdline=video=HDMI-A-1:1024x768@60e' >> /media/rootfs/boot/uEnv.txt"

Step-32:		cd /home/saif/EmbeddedLinux_Wandboard

Step-33:		sudo cp -v ./armv7-multiplatform/deploy/${kernel_version}.zImage /media/rootfs/boot/vmlinuz-${kernel_version}

Step-34:		sync

Step-35:		sudo mkdir -p /media/rootfs/boot/dtbs/${kernel_version}/

Step-36:		sudo tar xfv ./armv7-multiplatform/deploy/${kernel_version}-dtbs.tar.gz -C /media/rootfs/boot/dtbs/${kernel_version}/

Step-37:		sync

Step-38:		sudo tar xfv ./armv7-multiplatform/deploy/${kernel_version}-modules.tar.gz -C /media/rootfs/

Step-39:		sync

Step-40:		sudo sh -c "echo '/dev/mmcblk2p1  /  auto  errors=remount-ro  0  1' >> /media/rootfs/etc/fstab"

Step-41:		sync

Step-42:		eject the memory card

Step-43:		check the COM port detected.

Step-44:		sudo picocom -b 115200 /dev/ttyUSB0

Step-45:		Power On the target board.