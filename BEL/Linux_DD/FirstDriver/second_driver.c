#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>

//Called on insertion
//__init function will be cleaned up after the function call, if this module is statically build

extern char* name;

static int mfd_init(void) /* Constructor */
{
	printk(KERN_INFO "mfd registered, %s\n", name); //view it using dmesg
	return 0;
}

//Called on removal
static void __exit mfd_exit(void) /* Destructor */
{
	printk(KERN_INFO "mfd unregistered\n");
}

module_init(mfd_init);
module_exit(mfd_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Shaik Saifulla <shaik4consulting@gmail.com>");
MODULE_DESCRIPTION("My First Driver");
