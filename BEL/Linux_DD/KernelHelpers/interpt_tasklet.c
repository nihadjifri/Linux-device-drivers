#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/device.h>
#include <asm/uaccess.h>
#include <linux/uaccess.h>
#include <linux/interrupt.h>

#define FIRST_MINOR 0
#define MINOR_CNT 1
#define IRQ_NUM 12

//////////////bottom half////////////////////////
char tasklet_data[]="We use a string; but it could be pointer to a structure";

/* Tasklet handler, that just print the data */
void tasklet_function( unsigned long data )
{
    pr_info( "%s\n", (char *)data );
    return;
}

DECLARE_TASKLET( my_tasklet, tasklet_function, (unsigned long) tasklet_data );
//////////////////////////////////////////////////
char k_buff[50],*p;
int i_count;
////top half
static irq_handler_t irq_handler(int irq, void* dummy,struct pt_regs *regs)
{
	///////top half schedules here/////////
	tasklet_schedule( &my_tasklet );
	printk("Interrupt generated using mouse %d times\n",i_count);
	i_count++;
	return (irq_handler_t) IRQ_HANDLED;

}

static int my_open( struct inode * i, struct file * f)
{
	int ret_value;
	printk("Inside my open module\n");

	if(( ret_value = request_irq(IRQ_NUM,(irq_handler_t) irq_handler,IRQF_SHARED,"EMB",(void*) (irq_handler))))
	{
		printk("return_value = %d\n",ret_value);
		printk("Error in registering interrupt handler\n");
		return -1;
	}
	return 0;
}

static int my_close(struct inode * i, struct file *f)
{
	printk("Inside my close module\n");
//	free_irq(IRQ_NUM,(void*) (irq_handler));
	return 0;
}

static ssize_t my_read(struct file* f, char __user* buf, size_t size, loff_t* off)
{
	printk("\nInside my read function\n");	
	printk("Read: The content in kernel buffer is:	 %s\n",k_buff);
	copy_to_user(buf,k_buff,size);
	return 0;
}

static ssize_t my_write(struct file* f, const char __user* buf, size_t s, loff_t* off)
{
	printk("\nInside my write function\n");
	printk("Write: The content to be written to device is: 	%s\n",buf);
	copy_from_user(k_buff,buf,s);
	printk("Write: The content in kernel buffer is: 	%s\n",k_buff);
	return 0;
}

static dev_t dev;
static struct file_operations fops = 
{
	.owner = THIS_MODULE,
	.open = my_open,
	.release = my_close,
	.read = my_read,
	.write = my_write,
};

static struct cdev c_dev;
static struct class *cl;
static int start(void)
{
	int ret;
	struct device *dev_ret;
	printk("Welcome to device drivers\n");
	printk("Inside start module\n");
	
	if((ret = alloc_chrdev_region(&dev,FIRST_MINOR,MINOR_CNT,"emb")) < 0)
	{
		printk("Error in allocation of device number\n");
		return ret;
	}	
	
	cdev_init(&c_dev, &fops);
	if((ret = cdev_add(&c_dev,dev,MINOR_CNT)) < 0)
	{
		printk("Error in adding character device\n");
		printk("Unallocating the device number\n");
		unregister_chrdev_region(dev,MINOR_CNT);
		return ret;
	}
	printk("Major number: %d\n",MAJOR(dev));
	printk("Minor number: %d\n",MINOR(dev));

	if(IS_ERR(cl = class_create(THIS_MODULE,"EMB")))
	{
		printk("Error in creation of classn\n");
		cdev_del(&c_dev);
		unregister_chrdev_region(dev,MINOR_CNT);
		return PTR_ERR(cl);
	}
	
	if(IS_ERR(dev_ret = device_create(cl,NULL,dev,NULL,"Emb0")))
	{
		printk("Error in creation of a device file\n");
		class_destroy(cl);
		cdev_del(&c_dev);
		unregister_chrdev_region(dev,MINOR_CNT);
		return PTR_ERR(dev_ret);
	}
	return 0;
}

static void stop(void)
{
/* Stop the tasklet before we exit */
    tasklet_kill( &my_tasklet );
///////////////////////////////////////////////
	printk("Bye device drivers\n");
	printk("Inside stop module\n");
	free_irq(IRQ_NUM,(void*) (irq_handler));
	device_destroy(cl,dev);
	class_destroy(cl);
	cdev_del(&c_dev);
	unregister_chrdev_region(dev,MINOR_CNT);
}

module_init(start);
module_exit(stop);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("AUTOMATIC INSERTION OF DEVICE FILE");
MODULE_AUTHOR("*****EMBEDDED TEAM @ UTL**********");

