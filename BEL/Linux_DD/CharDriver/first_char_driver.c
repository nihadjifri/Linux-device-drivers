#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>

static dev_t dev_num; // Global variable for the first device number
#define FIRST_MINOR 0
#define TOTAL_MINORS 3
#define DEVICE_NAME "jifri"
static int __init ofcd_init(void) /* Constructor */
{
	int ret;

	printk(KERN_INFO "Namaskar: ofd registered\n");
	if ((ret = alloc_chrdev_region(&dev_num, FIRST_MINOR, TOTAL_MINORS, DEVICE_NAME)) < 0)
	{
		return ret;
	}
	printk(KERN_INFO "<Major, Minor>: <%d, %d>\n", MAJOR(dev_num), MINOR(dev_num));
	return 0;
}

static void __exit ofcd_exit(void) /* Destructor */
{
	unregister_chrdev_region(dev_num, TOTAL_MINORS);
	printk(KERN_INFO "Alvida: ofd unregistered\n");
}

module_init(ofcd_init);
module_exit(ofcd_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Shaik Saifulla <shaik4consulting@gmail.com>");
MODULE_DESCRIPTION("Our First Character Driver");
