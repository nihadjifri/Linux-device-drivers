/*
 * rtc.c: real time clock 146818, reading time of day
*/

#include <linux/module.h>
#include <asm/io.h>

#define AddrReg 0x70
#define DataReg 0x71

int init_module()
{
  unsigned char second, minute, hour;
  unsigned char day, month, year;

  /* select the addr register by outputting reg number and read the value */
  outb(0x00, AddrReg);
  second = inb(DataReg);

  outb(0x02, AddrReg);
  minute = inb(DataReg);

  outb(0x04, AddrReg);
  hour = inb(DataReg);

  outb(0x07, AddrReg);
  day = inb(DataReg);

  outb(0x08, AddrReg);
  month = inb(DataReg);

  outb(0x09, AddrReg);
  year = inb(DataReg);

  /* display the time of day */
  printk("%c%c/%c%c/%c%c - %c%c:%c%c:%c%c\n",
			((day & 0xf0) >> 4) + 0x30,
			(day & 0x0f) + 0x30,
			((month & 0xf0) >> 4) + 0x30,
			(month & 0x0f) + 0x30,
			((year & 0xf0) >> 4) + 0x30,
			(year & 0x0f) + 0x30,
			((hour & 0xf0) >> 4) + 0x30,
			(hour & 0x0f) + 0x30,
			((minute & 0xf0) >> 4) + 0x30,
			(minute & 0x0f) + 0x30,
			((second & 0xf0) >> 4) + 0x30,
			(second & 0x0f) + 0x30
	);
  return 0;
}

void cleanup_module()
{
}

MODULE_LICENSE("GPL");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Saif");
