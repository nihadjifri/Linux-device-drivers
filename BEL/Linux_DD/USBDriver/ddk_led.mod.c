#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xe89b6b83, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xfb459f11, __VMLINUX_SYMBOL_STR(usb_deregister) },
	{ 0xafe8e494, __VMLINUX_SYMBOL_STR(usb_register_driver) },
	{ 0xfad9ee14, __VMLINUX_SYMBOL_STR(device_create) },
	{ 0xb90a1470, __VMLINUX_SYMBOL_STR(__class_create) },
	{ 0xa45fe9e1, __VMLINUX_SYMBOL_STR(cdev_add) },
	{ 0xd518655a, __VMLINUX_SYMBOL_STR(cdev_init) },
	{ 0x29537c9e, __VMLINUX_SYMBOL_STR(alloc_chrdev_region) },
	{ 0xdb7305a1, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x4f8b5ddb, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0xbbbbc15f, __VMLINUX_SYMBOL_STR(usb_control_msg) },
	{ 0x7485e15e, __VMLINUX_SYMBOL_STR(unregister_chrdev_region) },
	{ 0x8f2d819b, __VMLINUX_SYMBOL_STR(cdev_del) },
	{ 0x5b2f4bfc, __VMLINUX_SYMBOL_STR(class_destroy) },
	{ 0x542de5a3, __VMLINUX_SYMBOL_STR(device_destroy) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xbdfb6dbb, __VMLINUX_SYMBOL_STR(__fentry__) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("usb:v16C0p05DCd*dc*dsc*dp*ic*isc*ip*in*");

MODULE_INFO(srcversion, "3BF1D7CB603D36559F0738F");
