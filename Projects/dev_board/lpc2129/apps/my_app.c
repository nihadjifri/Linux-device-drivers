#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h> 
int main()
{
	int fd =  open("/dev/ttyS0",O_WRONLY);
	if(fd == -1)
	{
		perror("Open failed :");
		exit(1);
	}
	else
	{
		printf("Port open successfully\n");
		int wr =  write(fd,"h",1);
		if(wr == -1)
		{
			perror("Write failed :");
			exit(1);	
		}
		else
		{
			printf("data written successfully\n");
		}
	}
}
