
#define RS (1<<10)
#define RW (1<<12)
#define EN (1<<13)
#define DATA (0xff<<15)
#define LCD RS|RW|EN|DATA
void delay(int);
void lcd_data(int);
void lcd_cmd(int);
void lcd_init(void);
void lcd_str(char *);
void lcd_long_str(char *);
void lcd_num(int);

void uart_write_long_str(char *);
char *uart_read_long_str(void);
//void uart_read_long_str(void);
