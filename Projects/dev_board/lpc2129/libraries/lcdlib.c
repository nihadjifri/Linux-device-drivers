#include <lpc21xx.h>
#include <string.h>
#include "./../headers/libheader.h"
#include "./../headers/lcdheader.h"

void lcd_cmd(int c)
{
	IOCLR0 = (0xff<<15);
	IOSET0 = (c<<15);
	IOCLR0 = RS;		//select command resister
	IOCLR0 = RW;		//select write operation
	IOSET0 = EN;		// setting high to low transition on EN pin
	delay(50);
	IOCLR0 = EN;		
}
void lcd_init(void)
{
	IODIR0 = IODIR0|LCD;
	lcd_cmd(0x38);
	lcd_cmd(0x0e);
	lcd_cmd(0x80);
	lcd_cmd(0x01);
}
void lcd_data(int data)
{
	IOCLR0 = (0xff<<15);
	IOSET0 = (data<<15);
	IOSET0 = RS;		//select data resister
	IOCLR0 = RW;		//select write operation
	IOSET0 = EN;		// setting high to low transition on EN pin
	delay(50);
	IOCLR0 = EN;	
}
void lcd_str(char *str)
{
	while(*str)
	{
		lcd_data(*str);
		str++;
	}
}
void lcd_long_str(char *str)
{
	int count = 0;
	while(*str)
	{
		if(count < 16)
		{
			lcd_data(*str);
			str++;
			count++;
		}
		else
		{
			lcd_cmd(0xc0);
			count=0;
		}
	}
}
void uart_write_long_str(char *str)
{
	//int count = strlen(str);
	while(*str)
	{
		while(!(U0LSR & (1<<5)))
		{}// Wait till old data omited
			U0THR = *str;
		str++;
	}
}
//char *uart_read_long_str()
//{
//	char *rcv;
//	int count = 0;
//	while( !(U0LSR & (1<<0)))
//		{}// Wait until RBR contains valid data
//	while(U0RBR != 13)
//	{	count++;
//		lcd_data('a');
//		while( !(U0LSR & (1<<0)))
//		{}// Wait until RBR contains valid data
//			*rcv=U0RBR;
//		lcd_data(*rcv);
//			rcv++;
//		
//		
//	}
//	return rcv-(count+1);
//}
char rcv[100];
char newString[10][10];
int string_to_words()
{
//char str1[100];
    //char newString[10][10]; 
    int i,j,ctr;
     /*  printf("\n\n Split string by space into words :\n");
       printf("---------------------------------------\n");    
 
    printf(" Input  a string : ");
    fgets(str1, sizeof str1, stdin);	
 */
    j=0; ctr=0;
    for(i=0;i<=(strlen(rcv));i++)
    {
        // if space or NULL found, assign NULL into newString[ctr]
        if(rcv[i]==' '||rcv[i]=='\0')
        {
            newString[ctr][j]='\0';
            ctr++;  //for next word
            j=0;    //for next word, init index to 0
        }
        else
        {
            newString[ctr][j]=rcv[i];
            j++;
        }
    }
   /* printf("\n Strings or words after split by space are :\n");
    for(i=0;i < ctr;i++)
        printf(" %s\n",newString[i]);
 
*/		return 0;
}
char *addition(char *a,char *b)
{
	//char sum[10];
	lcd_long_str("sum");
	return 0;
}

char *uart_read_long_str()
{
	//static char rcv[50];
	memset(rcv,0,100);
	int i = 0;
//	char c='';
//	while( !(U0LSR & (1<<0)))
//		{}// Wait until RBR contains valid data
	do
	{	
//			count++;
//		lcd_data('a');
		while( !(U0LSR & (1<<0)))
		{}// Wait until RBR contains valid data
			rcv[i]=U0RBR;
			//		lcd_data(rcv[i]);
		while(!(U0LSR & (1<<5)))
		{}// Wait till old data omited
			
			U0THR =rcv[i];
			i++;
		
		
	}while(13 != rcv[i-1] );
//	return rcv-(count+1);
	//lcd_long_str(rcv);
	if(!string_to_words())
	{
			if(!strncmp(newString[0],"echo",4))
			{
				//lcd_long_str(newString[1]);
				return newString[1];
			}
			else if(!strncmp(newString[0],"print",5))
			{
			lcd_long_str(newString[1]);
				return newString[0];
				//return 0;
			}
			else if(!strncmp(newString[0],"clear",5))
			{
			lcd_init();
				return newString[0];
				//return 0;
			}
			else if(!strncmp(newString[0],"expr",4))
			{
						//lcd_long_str(newString[1]);
						switch (newString[2][0])
						{
							case '+':
								addition(newString[1],newString[3]);
								break;
						}
							return newString[0];
							//return "expr";
			}
			else if (!strncmp(newString[0],"led",3))
			{
					//char ch=newString[1][0];
					IODIR1 |= (1<<17)|(1<<18)|(1<<19)|(1<<20)|(1<<21)|(1<<22)|(1<<23)|(1<<24);
				IOCLR1 |= (1<<17)|(1<<18)|(1<<19)|(1<<20)|(1<<21)|(1<<22)|(1<<23)|(1<<24);
				if (!strncmp(newString[1],"off",3))
					IOCLR1 |= (1<<17)|(1<<18)|(1<<19)|(1<<20)|(1<<21)|(1<<22)|(1<<23)|(1<<24);
				else if (!strncmp(newString[1],"on",2))
					IOSET1 |= (1<<17)|(1<<18)|(1<<19)|(1<<20)|(1<<21)|(1<<22)|(1<<23)|(1<<24);
				else
				{
					lcd_long_str("CNotFound");
				}
			}
			else
			{
				lcd_long_str("Command not found");
			}
	}
	else
	{
		lcd_long_str("convertion Failed");
	}//lcd_data();
	return rcv;
}

//void uart_read_long_str()
//{
////	char rcv[50];
//	strcpy(rcv,"\0");
//	int i = 0;
////	char c='';
////	while( !(U0LSR & (1<<0)))
////		{}// Wait until RBR contains valid data
//	do
//	{	
////			count++;
////		lcd_data('a');
//		while( !(U0LSR & (1<<0)))
//		{}// Wait until RBR contains valid data
//			rcv[i]=U0RBR;
//					lcd_data(rcv[i]);
//		while(!(U0LSR & (1<<5)))
//		{}// Wait till old data omited
//			
//			U0THR =rcv[i];
//			i++;
//		
//		
//	}while(13 != rcv[i-1] );
////	return rcv-(count+1);
//	lcd_long_str(rcv);
////	return rcv;
//}
void lcd_num(int num)
{
	if(num)
	{
		lcd_num(num/10);
		lcd_data((num%10) + 48);
	}
}
