#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

#define base 0x3F8           /* printer port base address */
#define value 65           /* numeric value to send to printer port */

static int my_module_init(void)		/*constructor*/
{
	printk(KERN_ALERT "My_module: Loading Module\n");
	outb(value, base);
	return 0;
}

static void my_module_exit(void)	/*destructor*/
{
	printk(KERN_ALERT "My_module: Unloading Module\n");
}

module_init(my_module_init);
module_exit(my_module_exit);

MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("Our First Driver");
MODULE_AUTHOR("Nihad jifri <nihadjifri@gmail.com>");
