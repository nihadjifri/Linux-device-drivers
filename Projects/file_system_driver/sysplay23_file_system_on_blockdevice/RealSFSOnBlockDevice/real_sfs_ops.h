#ifndef REAL_SFS_OPS_H
#define REAL_SFS_OPS_H

#include "real_sfs_ds.h"

/*
 * Inode number 0 is not treated as a valid inode number at many places,
 * including applications like ls. So, the inode numbering should start
 * at not less than 1. For example, making root inode's number 0, leads
 * the lookuped entries . & .. to be not shown in ls
 */
#define ROOT_INODE_NUM (1)

int init_browsing(sfs_info_t *info);
void shut_browsing(sfs_info_t *info);

#endif
