Here we need to discuss two concepts:

1: How to access command line arguments in a module
2: How to work with multiple files for a module

1:
 We should provide the command line arguments while we insert our module.

lets take an example:

	#include <linux/init.h>
	#include <linux/module.h>
	#include <linux/kernel.h>
	
	MODULE_LICENSE("Dual BSD/GPL");
	static char *whom = "world";
	static int howmany = 1;
	module_param(howmany,int,S_IRUGO);	//read permission for user,group & others
	module_param(whom,charp,S_IWUSR);	//write permission for owner
	
	static int hello_init(void)
	{
		int i;
		for(i=0;i<howmany;i++)
			printk(KERN_ALERT "(%d)Hello, %s\n",i,whom);
		return 0;
	}
	
	static void hello_exit(void)
	{
		printk(KERN_ALERT "Goodbye\n");
	}
	
	module_init(hello_init);
	module_exit(hello_exit);
	
	MODULE_AUTHOR("Nihad jifri <nihadjifri@gmail.com>");
	MODULE_DESCRIPTION("Module parameter test");

program to Demo how to pass command line input to the driver at load time
follow the steps discribed in the previous directory (README.md) to insert and remove module.
syntax :
$sudo insmod hellop.ko howmany=5 whom="alexa"

Try modinfo hello.ko
$ modinfo hellop.ko
filename:       /home/nihadjifri/Gitlab/Linux-device-drivers/module1_first_device_driver/parameter/hellop.ko
description:    Module parameter test
author:         Nihad jifri <nihadjifri@gmail.com>
license:        Dual BSD/GPL
srcversion:     A41D7FA8033EC7F55817985
depends:        
name:           hellop
vermagic:       4.13.0-45-generic SMP mod_unload 686 
parm:           howmany:int
parm:           whom:charp


Also check the /sys/module/hellop/parameters.

nihadjifri@nihadjifri-virtual-machine:/sys/module/hellop/parameters$ ls -al
total 0
drwxr-xr-x 2 root root    0 Jul  5 10:58 .
drwxr-xr-x 6 root root    0 Jul  5 10:52 ..
-r--r--r-- 1 root root 4096 Jul  5 10:58 howmany
--w------- 1 root root 4096 Jul  5 10:58 whom
nihadjifri@nihadjifri-virtual-machine:/sys/module/hellop/parameters$ cat howmany 
5
root@nihadjifri-virtual-machine:/sys/module/hellop/parameters# cat whom
alexa
