Hi all lets begin with our very first driver/module. we already know that our operating system is made of n number of modules, just like our Process management module, memory management, file management, I/O managment etc and the Linux device drivers. here we wanna know how they are build internally. if we talk specifically about the devices we have seen lots of different types of device:

charecter devices	[accessed as a stream of bytes]
block devices		[accessed as multiples of blocks]
Network devices		[make use of data packets]
Pseudo devices		[can be char/block/n/w driver but without any hardware eg: pseudo terminal - pty]

so lets see how to write a simple module first, irrespective of whether its a block, char or any other. here we are using memory as the hardware base of the device, that allows anyone to run the sample code without the need to acquare a special hardware.
we already know that module is a program that is dynamically linked to kernel and running in kernel space. so that we can't use our c funcitons or c libraries,system calls in kernel space and we don't have any thing like our main() function from where our normal c programme starts its excecution.

One intresting fact about the kernel is that it is an object-oriented implementation in C. Any linux driver has a constructor and destructor.The module's constructor is called when the module is successfully loaded into the kernel, and the destroctor is called when we unload the module from kernel. lets write our first driver/module, lets name it "my_module"

varsity@varsity-desktop:~/ldd/m1$ vi my_module.c

	#include <linux/init.h>
	#include <linux/module.h>
	#include <linux/kernel.h>
	
	static int my_module_init(void)		/*constructor*/
	{
		printk(KERN_ALERT "My_module: Loading Module\n");
		return 0;
	}
	
	static void my_module_exit(void)	/*destructor*/
	{
		printk(KERN_ALERT "My_module: Unloading Module\n");
	}
	
	module_init(my_module_init);
	module_exit(my_module_exit);
	
	MODULE_LICENSE("Dual BSD/GPL");
	MODULE_AUTHOR("Nihad jifri <nihadjifri@gmail.com>");
	MODULE_DESCRIPTION("Our First Driver");
	


 Here the module defines two functions, one to be invoked when the module is loaded into the kernel (hello_init)and one for when the module is removed (hello_exit). The module_init and module_exit lines use special kernel macros to indicate the role of these two functions. Another special macro (MODULE_LICENSE)is used to tell the kernel that this module bears a free license; without such a declaration, the kernel complains when the module is loaded.

To build a Linux driver, you need to have the kernel source (or, at least, the kernel headers) installed on your system. inorder to setup the kernel source tree checkout the "settingup_kernel_source_tree.txt" file in the same directory.

Now Its the time to compile it and create the module file my_module.ko. We use the kernel build system to do this. The Makefile invokes the kernel�s build system from the kernel source, and the kernel�s Makefile will, in turn, invoke our first driver�s Makefile to build our first driver.

lets create our make file:
varsity@varsity-desktop:~/ldd/m1$ vi Makefile

	obj-m := my_module.o
	KDIR := /lib/modules/$(shell uname -r)/build
	PWD :=  $(shell pwd)
	
	default:
	        $(MAKE) -C $(KDIR) SUBDIRS=$(PWD) modules
	
	clean:
	        $(MAKE) -C $(KDIR) SUBDIRS=$(PWD) clean

Comiling module:
----------------
once the C code and the Makefile is ready.all we need to do is to invoke "make" to build our first driver(my_module.ko)

varsity@varsity-desktop:~/ldd/m1$ make
make -C /lib/modules/4.2.0-42-generic/build SUBDIRS=/home/varsity/ldd/m1 modules
make[1]: Entering directory `/usr/src/linux-headers-4.2.0-42-generic'
  CC [M]  /home/varsity/ldd/m1/my_module.o
  Building modules, stage 2.
  MODPOST 1 modules
  CC      /home/varsity/ldd/m1/my_module.mod.o
  LD [M]  /home/varsity/ldd/m1/my_module.ko
make[1]: Leaving directory `/usr/src/linux-headers-4.2.0-42-generic'
varsity@varsity-desktop:~/ldd/m1$ ls
Makefile  modules.order  Module.symvers  my_module.c  my_module.ko  my_module.mod.c  my_module.mod.o  my_module.o
varsity@varsity-desktop:~/ldd/m1$


now we will get the <module_name>.ko file.

Inserting module to kernel:
---------------------------

$ sudo insmod my_module.ko

Now check the kernel message buffer:
------------------------------------
$ dmesg | tail -5

[   49.677410] audit: type=1400 audit(1530761337.395:32): apparmor="STATUS" operation="profile_replace" profile="unconfined" name="/usr/sbin/cupsd" pid=2180 comm="apparmor_parser"
[  108.542417] systemd-hostnamed[2319]: Warning: nss-myhostname is not installed. Changing the local hostname might make it unresolveable. Please install nss-myhostname!
[ 1616.534423] perf interrupt took too long (2508 > 2500), lowering kernel.perf_event_max_sample_rate to 50000

[ 1891.993233] my_module: module verification failed: signature and/or required key missing - tainting kernel

[ 1891.993515] My_module: Loading Module

To remove module from kernel :
------------------------------
$ sudo rmmod my_module

Check for the updated kernel messages :
---------------------------------------
$ dmesg | tail -5

[  108.542417] systemd-hostnamed[2319]: Warning: nss-myhostname is not installed. Changing the local hostname might make it unresolveable. Please install nss-myhostname!

[ 1616.534423] perf interrupt took too long (2508 > 2500), lowering kernel.perf_event_max_sample_rate to 50000

[ 1891.993233] my_module: module verification failed: signature and/or required key missing - tainting kernel

[ 1891.993515] My_module: Loading Module

[ 1949.902764] My_module: Unloading Module

if we want to remove all the temperory files generated during compilation of our module:
-----------------------------------------------------------------------------------------
$ ls

Makefile       Module.symvers  my_module.c   my_module.mod.c  my_module.o  procfs.c   sysfs.c

modules.order  multi_file      my_module.ko  my_module.mod.o  parameter    README.md

varsity@varsity-desktop:~/Desktop/Gitlab/Linux-device-drivers/module1_first_device_driver$ make clean

make -C /lib/modules/4.2.0-42-generic/build SUBDIRS=/home/varsity/Desktop/Gitlab/Linux-device-drivers/module1_first_device_driver clean

make[1]: Entering directory `/usr/src/linux-headers-4.2.0-42-generic'

  CLEAN   /home/varsity/Desktop/Gitlab/Linux-device-drivers/module1_first_device_driver/.tmp_versions

  CLEAN   /home/varsity/Desktop/Gitlab/Linux-device-drivers/module1_first_device_driver/Module.symvers

make[1]: Leaving directory `/usr/src/linux-headers-4.2.0-42-generic'

varsity@varsity-desktop:~/Desktop/Gitlab/Linux-device-drivers/module1_first_device_driver$ ls

Makefile  multi_file  my_module.c  parameter  procfs.c  README.md  sysfs.c

