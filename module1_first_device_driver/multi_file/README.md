Here we discuss how to work with multiple files for a single module.
how to parse arguments to a function whose defenition is written in 
another file. or in other words how to deal with dependent modules.

In this directory you can find two .c files in which module "hello" is
depending module "add" to find the defenition for the function add.

here your module "hello" depends on module "add", so while inserting 
those modules we should make sure that the module add should be inserted
prior to module hello, other wise it will lead to some error.

So here we can do it in two methords:

1: insert both modules one after the other as per the dependancy order.
   by making use of insmod command.
2: we can make use of another command called modprobe here:

   inorder to use modprobe we should make a copy/link of both the module
   files (hello.ko,add.ko) to /lib/modules/`uname -r`

So lets compile our module first: 
have a look into the Makefile, here we are compiling both the modules together.


invoke make:
nihadjifri@nihadjifri-virtual-machine:~/Gitlab/Linux-device-drivers/module1_first_device_driver/multi_file$ make
make -C /lib/modules/`uname -r`/build M=`pwd` modules
make[1]: Entering directory '/usr/src/linux-headers-4.13.0-45-generic'
  CC [M]  /home/nihadjifri/Gitlab/Linux-device-drivers/module1_first_device_driver/multi_file/hello.o
  CC [M]  /home/nihadjifri/Gitlab/Linux-device-drivers/module1_first_device_driver/multi_file/add.o
  Building modules, stage 2.
  MODPOST 2 modules
  CC      /home/nihadjifri/Gitlab/Linux-device-drivers/module1_first_device_driver/multi_file/add.mod.o
  LD [M]  /home/nihadjifri/Gitlab/Linux-device-drivers/module1_first_device_driver/multi_file/add.ko
  CC      /home/nihadjifri/Gitlab/Linux-device-drivers/module1_first_device_driver/multi_file/hello.mod.o
  LD [M]  /home/nihadjifri/Gitlab/Linux-device-drivers/module1_first_device_driver/multi_file/hello.ko
make[1]: Leaving directory '/usr/src/linux-headers-4.13.0-45-generic'
nihadjifri@nihadjifri-virtual-machine:~/Gitlab/Linux-device-drivers/module1_first_device_driver/multi_file$ ls
add.c   add.mod.c  add.o    hello.ko     hello.mod.o  Makefile       Module.symvers
add.ko  add.mod.o  hello.c  hello.mod.c  hello.o      modules.order  README.md

Now create a link of both the .ko files to /lib/modules/`uname -r`/
make sure you should use absulute path of the source file.

$ sudo ln -s ~/Gitlab/Linux-device-drivers/module1_first_device_driver/multi_file/add.ko /lib/modules/`uname -r`
$ sudo ln -s ~/Gitlab/Linux-device-drivers/module1_first_device_driver/multi_file/add.ko /lib/modules/`uname -r`

verify whether our links are created successfully

$ find /lib/modules/`uname -r`/ -type l -name "*.ko"

/lib/modules/4.13.0-45-generic/add.ko
/lib/modules/4.13.0-45-generic/hello.ko

Now to run modprobe at first run depmod -a command which will calculate the module dependancy for all the modules
in /lib/modules/`uname -r` directory.

$sudo depmod -a

The best advantage of modprobe is that it can be called from any location in your system
but to use insmod command we should be in the same directory where our .ko file lies.

now invoke modprobe with the module name "hello" (without .ko) it will automatically insert all the moodules 
that it depends on too.

$sudo modprobe hello

now if we check the currently available modules in the kernel we will see both hello and add is added there.

$ lsmod | head -5
Module                  Size  Used by
hello                  16384  0
add                    16384  1 hello
nls_utf8               16384  1
isofs                  40960  1

here you can also notice that for module "add" its also telling you that its been used by module "hello"

now if we check the kernel messages we can see that we are getting sum which is calculated from add module:
$ dmesg | tail -5
[ 1798.765674] hellop: (3)Hello, alexa
[ 1798.765677] hellop: (4)Hello, alexa
[ 7282.902448] hellop: Goodbye
[ 7476.656168] Hello : Hello World
[ 7476.656178] Hello : 30

now when you remove these modules make sure that you have to remove hello first other wise it will give you error

$ sudo rmmod add
rmmod: ERROR: Module add is in use by: hello

$ sudo rmmod hello
$ sudo rmmod add
$

Now go ahead and play around with this. try to impliment different applications, and get back to me if you
find any difficulty.

Once you are done, clear all your temperory files using make:
$ ls
add.c   add.mod.c  add.o    hello.ko     hello.mod.o  Makefile       Module.symvers
add.ko  add.mod.o  hello.c  hello.mod.c  hello.o      modules.order  README.md

$ make clean
make -C /lib/modules/`uname -r`/build M=`pwd` clean
make[1]: Entering directory '/usr/src/linux-headers-4.13.0-45-generic'
  CLEAN   /home/nihadjifri/Gitlab/Linux-device-drivers/module1_first_device_driver/multi_file/.tmp_versions
  CLEAN   /home/nihadjifri/Gitlab/Linux-device-drivers/module1_first_device_driver/multi_file/Module.symvers
make[1]: Leaving directory '/usr/src/linux-headers-4.13.0-45-generic'

$ ls
add.c  hello.c  Makefile  README.md

