//filename : hello.c

#include<linux/init.h>
#include<linux/module.h>
#include<linux/kernel.h>

int add(int,int);

int a=10;
int b=20;
EXPORT_SYMBOL(a);
EXPORT_SYMBOL(b);
static int hello_init(void)
{
	printk(KERN_ALERT "Hello : Hello World\n");
	printk("Hello : %d\n",add(a,b));
	return 0;
}

static void hello_exit(void)
{
	printk("Hello :Goodbye\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("Dependent module");
MODULE_AUTHOR("Nihad jifri <nihadjifri@gmail.com>");

