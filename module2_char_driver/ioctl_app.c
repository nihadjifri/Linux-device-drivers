#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#define PCD_MAGIC 'X'
#define PCD_GET_CUR_FS_SZ _IOR(PCD_MAGIC,1,int)
#define PCD_GET_MAX_FS_SZ _IOR(PCD_MAGIC,2,int)

int main()
{
	int fd,wr,file_size=0;
	fd=open("/dev/char_dev",O_WRONLY);
	if(fd < 0)
	{
		printf("file open error\n");
		exit(-1);
	}
		ioctl(fd,PCD_GET_CUR_FS_SZ,&file_size);
		printf("current size = %d\n",file_size);
		
		printf("fd = %d\n",fd);
		wr=write(fd,"hello",5);
		printf("written bytes %d\n",wr);

		ioctl(fd,PCD_GET_MAX_FS_SZ,&file_size);
		printf("current size = %d\n",file_size);
	
		ioctl(fd,PCD_GET_CUR_FS_SZ,&file_size);
		printf("current size = %d\n",file_size);
		

}
