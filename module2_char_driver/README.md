Hi, Now lets begin with charecter device driver. we already know that
charecter devices are those devices that do data transfer by charecters.
eg: Keyboard, mouse, terminal, parallel port etc.. either it can be an
actual device or a pseudo device.

Anyhow Linux identifies the devices and the drives with the help of
device numbers:

1: Major number	-> identifies the driver associated with the device
2: Minor number -> identifies the device

below you can observe a scenario when i have connected two usb keyboards
to my system.
here both the keyboard uses the same driver usbhid with major number 248
and individual keyboard takes different minor numbers 3,4.

$ls -al /dev
> crw-------   1 root root    248,   3 Jul  5 17:39 hidraw3
> crw-------   1 root root    248,   4 Jul  5 17:39 hidraw4

$usb-devices
T:  Bus=05 Lev=01 Prnt=01 Port=01 Cnt=02 Dev#=  3 Spd=1.5 MxCh= 0
D:  Ver= 1.10 Cls=00(>ifc ) Sub=00 Prot=00 MxPS= 8 #Cfgs=  1
P:  Vendor=04ca ProdID=004b Rev=01.09
S:  Manufacturer=Lite-On Technology Corp.
S:  Product=USB Keyboard
C:  #Ifs= 2 Cfg#= 1 Atr=a0 MxPwr=100mA
I:  If#= 0 Alt= 0 #EPs= 1 Cls=03(HID  ) Sub=01 Prot=01 Driver=usbhid
I:  If#= 1 Alt= 0 #EPs= 1 Cls=03(HID  ) Sub=00 Prot=00 Driver=usbhid

T:  Bus=03 Lev=01 Prnt=01 Port=01 Cnt=01 Dev#=  2 Spd=1.5 MxCh= 0
D:  Ver= 1.10 Cls=00(>ifc ) Sub=00 Prot=00 MxPS= 8 #Cfgs=  1
P:  Vendor=046d ProdID=c31c Rev=64.00
S:  Manufacturer=Logitech
S:  Product=USB Keyboard
C:  #Ifs= 2 Cfg#= 1 Atr=a0 MxPwr=90mA
I:  If#= 0 Alt= 0 #EPs= 1 Cls=03(HID  ) Sub=01 Prot=01 Driver=usbhid
I:  If#= 1 Alt= 0 #EPs= 1 Cls=03(HID  ) Sub=00 Prot=00 Driver=usbhid

$ lsmod | grep "usbhid"
usbhid                 49152  0 
hid                    98304  2 hid_generic,usbhid
 now if we need to all the majornumbers used in our system have a look at

/proc/devices.
$cat /proc/devices

Every device on our system will have a 32 bit device number of the type 
(dev_t - defined in <linux/types.h>) (which is a compination of 12 bits 
major number and 20 bits minor number).

so if we have a device number inoder to extract the major number and minor
number from it we will should make use of macros defined in <linux/kdev_t.h>

MAJOR(dev_t dev);
MINOR(dev_t dev);

	OR

if we have the major and the minor number and need to turn them into 
a device number (dev_t dev) we use :

MKDEV(int major, int minor);

Now we need to have device node to access our device from user applications.
and a specific driver should be connected to it make it functional.

 Here we have two ways to create our device node.
Method 1: Creating device node manually using commands:
          the command to create a device file / device node is  mknod :
          $sudo mknod device_name device-type major-number minor-number
          eg:$sudo mknod mydev c 254 0

Method 2: Automatic creation of device node.
  The automatic creation of device files are done by udev daemon, and it
will update the appropriate device class and device information into /sys window,
inorder to make it happen we need to use some of the Linux device model APIs
declared in <linux/device.h>. the rest will be handled by udev.

 As first the device class is created using the API:
	struct class *cl = class_create(THIS_MODULE, "<device class name>");
 Now the device informations such as major and minor numbers are provided
using the API:
	device_create(cl, NULL, first, NULL, "<device name format>", ...);

 Now we need to destroy both while we remove the module :
        device_destroy(cl,first);
        class_destroy(cl);

In case of multiple minors (eg: pendrive with multiple partition), the 
device_create() and device_destroy() APIs may be put in the for loop, 
and the <device name format> string could be useful. 

Eg:The device_create() call in a for loop indexed by i could be as follows:

device_create(cl, NULL, MKNOD(MAJOR(first), MINOR(first) + i), NULL, "mynull%d", i);

Any how it is not recomented to create device node first. lets allocate a major 
and minor number for our driver.

it can be done in two ways:

1: Static :
 int regirster_chrdev_region(dev_first,unsigned int count,char *name); //*
  first = begnning device number.
  count = total number of contigous device numbers.
  name  = a name to display on /proc/devices , sysfs
 Return : success : 0 , failure : -ve error code

2:Dynamic :
 int alloc_chrdev_region(dev_t *dev, unsigned int firstminor,unsigned int count,char *name);
  dev = is an output only parameter that will hold the first number in the allocated
  range on successfull completion.

we need to do this registration during the init part of our device driver.
and similarly we should unregister the same in the cleanup or exit funtion of our driver.

Once we have got the device number, we need to register the
 our character device. Kernel uses structure of the type struct cdev
 to represent the character devices internally

 There are two ways to Allocate and initialize our structure:
      The second methord (Old methord) doesn't make use of cdev structure
      and no longer used cuz new code should use never techniqe 
      (reference: LDD3.pdf - Jonathan Corbet pg: 55)
 Methord1:
 a) If we want to Obtaining standalone cdev structure at runtime
              struct cdev *my_cdev = cdev_alloc();
              my_cdev->ops = &my_fops
 b) Embed the cdev structure within a device specific struture of your own
 c) Initialize the structure that you 
              void cdev_init(struct cdev *cdev,struct file_operations *fops);
 d) struct cdev has an owner field that should be set to THIS_MODULE
 e) Tell the kernel about it
              int cdev_add(struct cdev *cdev, dev_t num, unsigned int count);

 To Remove char device from the system
      void cdev_del(struct cdev *dev);

