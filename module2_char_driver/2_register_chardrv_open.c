/*
 * Once we have got the device number, we need to register the
 * our character device. Kernel uses structure of the type struct cdev
 * to represent the character devices internally
 *
 * There are two ways to Allocate and initialize our structure:
 *	The second methord (Old methord) doesn't make use of cdev structure
 * 	and no longer used cuz new code should use never techniqe 
 *	(reference: LDD3.pdf - Jonathan Corbet pg: 55)
 * Methord1:
 * a) If we want to Obtaining standalone cdev structure at runtime
 * 		struct cdev *my_cdev = cdev_alloc();
 *		my_cdev->ops = &my_fops
 * b) Embed the cdev structure within a device specific struture of your own
 * c) Initialize the structure that you 
 * 		void cdev_init(struct cdev *cdev,struct file_operations *fops);
 * d) struct cdev has an owner field that should be set to THIS_MODULE
 * e) Tell the kernel about it
 * 		int cdev_add(struct cdev *cdev, dev_t num, unsigned int count);
 *
 * To Remove char device from the system
 * 	void cdev_del(struct cdev *dev);

*/
#include<linux/init.h>
#include<linux/module.h> // THIS_MODULE <- file_operations.owner
#include<linux/cdev.h>	// struct cdev
#include<linux/slab.h>
#include<linux/kernel.h>
#include<linux/fs.h>
#include<linux/types.h>
#include<linux/uaccess.h>

dev_t  devno;  //to store the device no//
int scull_major=0;  // to store the major no//
int scull_minor=0;

struct scull_dev  //dev structure//
{
    char kbuf[35];
    int curr;
    int max_size;
    struct cdev cdev;
};
struct scull_dev mydev=
{
	.kbuf="\0",
	.curr=0,
	.max_size=35,
};
MODULE_LICENSE("DUAL BSD/GPL");
static int scull_open(struct inode *,struct file *);
static int scull_release(struct inode*,struct file *);
static int scull_open(struct inode *inode,struct file *filp)
{
      printk(KERN_INFO "open module\n");
      return 0;
}
static int scull_release(struct inode *inode,struct file *filp)
{
      printk(KERN_INFO "t_release module\n");
      return 0;
}
struct file_operations scull_fops=
{
	.owner=THIS_MODULE,
	.open=scull_open,
	.release=scull_release,
};
static int hello_init(void)
{
    int result;
    int dno=0;
    int err;
    printk(KERN_INFO "inserting module\n");
	if(scull_major)
	{
	       dno=MKDEV(scull_major,scull_minor);
               result=register_chrdev_region(dno,1,"char_drv1");
        }
       else
        {
              result=alloc_chrdev_region(&devno, scull_minor,1,"char_drv1");
              if(result>=0)
              {  
                     scull_major=MAJOR(devno);
		     scull_minor=MINOR(devno);
                     printk(KERN_WARNING "major %d minor %d \n",scull_major,scull_minor);
              }
        } 
     if(result<0)
    {
         printk(KERN_WARNING "scull can't get major\n");
         printk("dev size =%d\n",mydev.max_size);
         return -1;
    }
  cdev_init(&mydev.cdev,&scull_fops);
  mydev.cdev.owner=THIS_MODULE;
  err=cdev_add(&mydev.cdev,devno,1);
    if(err)
    {
       printk(KERN_NOTICE "error %d adding scull",err);
       return -1;
  
    }
 return 0;
}
static void hello_exit(void)
{
    printk(KERN_ALERT "Goodbye\n");
    cdev_del(&mydev.cdev);
    unregister_chrdev_region(devno,1);
}
module_init(hello_init);
module_exit(hello_exit);
MODULE_LICENSE("GPL");

