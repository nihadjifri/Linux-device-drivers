#include <linux/delay.h> /* usleep_range */
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/module.h>

MODULE_LICENSE("GPL");

static struct task_struct *kthread;

static int work_func(void *data)
{
    int i = 0;
    while (!kthread_should_stop()) {
        printk(KERN_INFO "%d\n", i);
        usleep_range(1000000, 1000001);
        i++;
        if (i == 10)
            i = 0;
    }
    return 0;
}

int init_module(void)
{
    kthread = kthread_create(work_func, NULL, "mykthread");
    wake_up_process(kthread);
    return 0;
}

void cleanup_module(void)
{
    /* Waits for thread to return. */
    kthread_stop(kthread);
}

/*
 *https://github.com/cirosantilli/linux-kernel-module-cheat/blob/9f6ddbc436f344dd77dd9d5646dbe643f97533e4/kernel_modules/timer.c
 *https://stackoverflow.com/questions/15994603/how-to-sleep-in-the-linux-kernel/15994682
 *https://stackoverflow.com/questions/39876457/how-to-sleep-in-the-linux-kernel-space/39921020#39921020
 * */
